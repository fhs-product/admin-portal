import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/appointment/appointments',
    method: 'get',
     params: query
  })
}

export function fetchAppointment(id) {
  return request({
    url: '/vue-element-admin/article/detail',
    method: 'get',
    params: { id }
  })
}

export function createAppointment(data) {
  return request({
    url: '/appointment/appointments',
    method: 'post',
    data
  })
}

export function deleteAppointment(id) {
  return request({
    url: '/appointment/appointments/' + id,
    method: 'delete'
  })
}

export function updateAppointment(id, data) {
  return request({
    url: '/appointment/appointments/' + id,
    method: 'put',
    data
  })
}
