import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/user/practitioners',
    method: 'get',
     params: query
  })
}

export function fetchArticle(id) {
  return request({
    url: '/user/practitioners/' + id,
    method: 'get',
    params: { id }
  })
}

export function fetchPv(pv) {
  return request({
    url: '/vue-element-admin/article/pv',
    method: 'get',
    params: { pv }
  })
}

export function createArticle(data) {
  return request({
    url: '/user/practitioners',
    method: 'post',
    data
  })
}

export function deleteArticle(id) {
  return request({
    url: 'https://akahealth-appoitment-service.herokuapp.com/appointments/' + id,
    method: 'delete'
  })
}

export function updateArticle(id, data) {
  return request({
    url: 'https://akahealth-appoitment-service.herokuapp.com/appointments/' + id,
    method: 'put',
    data
  })
}
