import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/iam/auth/login',
    method: 'post',
    data
  })
}

export function getInfo(token) {
  return request({
    url: '/iam/auth/tokenInfo',
    method: 'get'
    // params: { token }
  })
}

export function logout() {
  return request({
    url: '/iam/auth/logout',
    method: 'get'
  })
}
