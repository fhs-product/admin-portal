import request from '@/utils/request'

export function getList(params) {
  return request({
    url: 'https://akahealth-appoitment-service.herokuapp.com/appointments',
    method: 'get',
    params
  })
}
